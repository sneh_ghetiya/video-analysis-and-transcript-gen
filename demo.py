# importing libraries 
import speech_recognition as sr 
import os
import subprocess
from sys import stdout, argv
import shutil
from pydub import AudioSegment
from pydub.silence import split_on_silence

# create a speech recognition object
r = sr.Recognizer()

# Convert video to audio(wav)
def convertToAudio(filename: str): 
    path = './{0}.mp4'.format(filename)
    audio_path = os.path.dirname(path) + '/' + filename.split('.')[0] + '.wav'
    if (not os.path.exists(audio_path)):
        subprocess.call(['ffmpeg', '-i', path, '-codec:a', 'pcm_s16le', '-loglevel', 'error', '-ac', '1', audio_path])
    get_large_audio_transcription(audio_path);


# a function that splits the audio file into chunks
# and applies speech recognition
def get_large_audio_transcription(path: str):
    """
    Splitting the large audio file into chunks
    and apply speech recognition on each of these chunks
    """
    print('Converting audio to transcripts...')
    # open the audio file using pydub
    sound = AudioSegment.from_wav(path)  
    # split audio sound where silence is 700 miliseconds or more and get chunks
    chunks = split_on_silence(sound,
        # experiment with this value for your target audio file
        min_silence_len = 550,
        # adjust this per requirement
        silence_thresh = sound.dBFS-17,
        # keep the silence for 1 second, adjustable as well
        keep_silence=200,
    )
    folder_name = "audio-chunks"
    # create a directory to store the audio chunks
    if not os.path.isdir(folder_name):
        os.mkdir(folder_name)
    whole_text = ""
    # process each chunk 
    total_chunks = len(chunks);
    for i, audio_chunk in enumerate(chunks, start=1):
        # export audio chunk and save it in
        # the `folder_name` directory.
        chunk_filename = os.path.join(folder_name, f"chunk{i}.wav")
        audio_chunk.export(chunk_filename, format="wav")
        # recognize the chunk
        with sr.AudioFile(chunk_filename) as source:
            audio_listened = r.record(source)
            # try converting it to text
            try:
                text = r.recognize_google(audio_listened, language="en-IN")
            except sr.UnknownValueError as e:
                print("Error:", str(e))
            else:
                text = f"{text.capitalize()}. "
                stdout.write("\rProgress: [{0:50s}] {1:.1f}%".format('#' * int((i / total_chunks) * 50), (i / total_chunks) * 100))
                whole_text = whole_text + text + '\n'

    print()
    with open('{0}.txt'.format(filename), 'w') as f:
        for line in whole_text:
            f.write(line)

    shutil.rmtree('./audio-chunks')

    # return the text for all chunks detected
    # return whole_text


# path = "interview.wav"
# print("\nFull text:", get_large_audio_transcription(path))
if (len(argv) == 1):
    print('No input file specified in arguments.', end='\n')
    filename = str(input('Please specify an input video file: '))
else:
    filename = str(argv[1])

if (len(filename.split('.')) > 1):
    filename = filename.split('.')[0]

convertToAudio(filename)
print('Audio transcription successful. See `{0}.txt`'.format(filename));