import subprocess
import os
import cv2
import time
import shutil
import numpy as np
import multiprocessing as mp
from sys import stdout, argv
from collections import Counter
from matplotlib import pyplot as plt
from tensorflow.keras.preprocessing import image
from tensorflow.keras.models import model_from_json

# video_path = './interview.mp4'
face_cascade = cv2.CascadeClassifier('../env/lib/python3.6/site-packages/cv2/data/haarcascade_frontalface_default.xml')
emotions = ('Angry', 'Disgust', 'Fear', 'Happy', 'Sad', 'Surprise', 'Neutral')
emotion_list = []

def processVideo(group_number):
    model = model_from_json(open('fer.json', 'r').read())
    model.load_weights('fer.h5') #load weights
    cap = cv2.VideoCapture(file_name)
    cap.set(cv2.CAP_PROP_POS_FRAMES, frame_jump_unit * group_number)
    width, height = (
        int(cap.get(cv2.CAP_PROP_FRAME_WIDTH)),
        int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
    )
    # no_of_frames = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
    fps = int(cap.get(cv2.CAP_PROP_FPS))
    processed_frames = 0

    fourcc = cv2.VideoWriter_fourcc('m', 'p', '4', 'v')
    out = cv2.VideoWriter()
    if (not os.path.exists('./output')):
        os.mkdir('./output')
    out.open('./output/output_{}.mp4'.format(group_number), fourcc, fps, (width, height), True)
    try:
        while processed_frames < frame_jump_unit:
            if (group_number == 1):
                stdout.write("\rProgress: [{0:50s}] {1:.1f}%".format('#' * int((processed_frames / frame_jump_unit) * 50), (processed_frames / frame_jump_unit) * 100))
            ret, img = cap.read()
            if not ret:
                break
            gray_img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
            faces = face_cascade.detectMultiScale(gray_img, 1.32, 5)
            # img = cv2.resize(img, (1000, 800))
            for (x,y,w,h) in faces:
                # if (group_number == 1):
                #     cv2.rectangle(img,(x,y),(x+w,y+h),(0,255,0), thickness = 2)
                face = gray_img[int(y):int(y+h), int(x):int(x+w)]
                face = cv2.resize(face, (48, 48))
                img_pixels = image.img_to_array(face)
                img_pixels = np.expand_dims(img_pixels, axis = 0)
                img_pixels = img_pixels/255
                predictions = model.predict(img_pixels)
                max_index = np.argmax(predictions[0])
                emotion = emotions[max_index]
                emotion_list.append(emotion)
                # if (group_number == 1):
                #     cv2.putText(img, emotion, (int(x), int(y)-10), 
                #                 cv2.FONT_HERSHEY_SIMPLEX, 1, 
                #                 color = (255,255,255),
                #                 thickness = 1)
                #     cv2.imshow('Emotion Recognizer', img)
            processed_frames += 1
    except:
        cap.release()
        # cv2.destroyAllWindows()
        out.release()
    cap.release()
    # cv2.destroyAllWindows()
    out.release()
    return Counter(emotion_list)

def multi_process():
    if (__name__ == '__main__'):
        start_time = time.time()
        p = mp.Pool(num_processes)
        output = p.map(processVideo, range(num_processes))
        for x in range(1, len(output)):
            output[0].update(output[x])
        shutil.rmtree('./output')
        end_time = time.time()
        duration = end_time - start_time
        print('\nTime taken: {0:.2f} minutes'.format(duration/60))
        # generate_transcript()
        makePie(output[0])
        # return ([{'name': key, 'value': float("{0:.1f}".format((value/frame_count)*100))} for key, value in output[0].items()])
        return (output[0])

def generate_transcript():
    subprocess.call(['python', 'demo.py', file_name])

def get_frame_count(filename):
    cap = cv2.VideoCapture(filename)
    fc = cap.get(cv2.CAP_PROP_FRAME_COUNT)
    cap.release()
    return fc

def makePie(c):
    plt.figure(num='Detected Emotions')
    plt.pie([float(v) for v in c.values()], labels=[k for k in c], autopct='%1.1f%%')
    plt.title("Emotions Detected throughout the Interview")
    plt.savefig(os.path.basename(file_name).split('.')[0] + '.png')
    plt.show()


if (len(argv) == 1):
    print('No input file specified in arguments.')
    file_name = str(input('Please specify an input video file: '))
else:
    file_name = str(argv[1])

if (len(file_name.split('.')) > 1):
    path = './{0}'.format(file_name)
else:
    path = './{0}.mp4'.format(file_name)

frame_count = get_frame_count(file_name)
print(frame_count)
# Set num_processes to the number of threads you want to create
num_processes = mp.cpu_count()
frame_jump_unit = int(frame_count // num_processes)
# print('Number of Processors: {0}\n Frame jump unit: {1}'.format(num_processes, frame_jump_unit))
result = multi_process()
print(result)
